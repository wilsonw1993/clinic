﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Mvc;
using Clinic.Models;

namespace Clinic.Controllers
{
    public class RecordsController : Controller
    {
        private ClinicModel model = new ClinicModel();

        public ActionResult InsertRecord(int id)
        {
            //Patient patient = model.Patients.Find(id);
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InsertRecord(int id, [Bind(Exclude = "Id")] Record record)
        {
            if (ModelState.IsValid)
            {
                Patient patient = model.Patients.Find(id);
                record.Patient = patient;
                model.Records.Add(record);
                model.SaveChanges();
                return RedirectToAction("Details", "Patients", new {id = id});
            }
            return RedirectToAction("Index", "Patients");
        }
        
    }
}
