﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clinic.Models;

namespace Clinic.Controllers
{
    public class PatientsController : Controller
    {
        private ClinicModel model = new ClinicModel();

        public ActionResult Index()
        {
            IEnumerable<Patient> patients =
                (from patient in model.Patients
                    orderby patient.LastName
                    select patient);
            return View(patients);
        }

        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Exclude = "Id")] Patient patient)
        {
            if (ModelState.IsValid)
            {
                model.Patients.Add(patient);
                model.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(patient);
        }

        public ActionResult Edit(int id)
        {
            Patient patient = model.Patients.Find(id);
            return View(patient);
        }

        [HttpPost]
        public ActionResult Edit(Patient patient)
        {
            if (ModelState.IsValid)
            {
                model.Entry(patient).State = EntityState.Modified;
                model.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(patient);
        }

        public ActionResult Delete(int id)
        {
            Patient patient = model.Patients.Find(id);
            model.Patients.Remove(patient);
            model.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Details(int id)
        {
            Patient patient = model.Patients.Find(id);
            return View(patient);
        }
    }
}
